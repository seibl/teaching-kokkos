#include <chrono>
#include <iostream>
#include <vector>

/**
 * Perform a vector times matrix times vector multiplication.
 */
void yAx()
{
    // dimensions
    constexpr size_t M = 65536;
    constexpr size_t N = 1024;
    constexpr size_t iterations = 100;
    /// solution of the multiplication if all elements are initialized to 1
    double solution = M * N;

    // vector y in R^M
    auto y = new double[M];
    // matrix A in R^MxN
    auto A = new double[M][N];
    // vector x in R^N
    auto x = new double[N];

    // initialization
    for (auto idx = 0; idx < M; ++idx)
    {
        y[idx] = 1;
    }
    for (auto idx = 0; idx < M; ++idx)
    {
        for (auto jdx = 0; jdx < N; ++jdx)
        {
            A[idx][jdx] = 1;
        }
    }
    for (auto jdx = 0; jdx < N; ++jdx)
    {
        x[jdx] = 1;
    }

    auto start = std::chrono::system_clock::now();
    for (auto i = 0; i < iterations; ++i)
    {
        double result = 0;
        for (auto idx = 0; idx < M; ++idx)
        {
            double tmp = 0;
            for (auto jdx = 0; jdx < N; ++jdx)
            {
                tmp += A[idx][jdx] * x[jdx];
            }
            result += y[idx] * tmp;
        }
        if (std::abs(result - solution) >1e-8)
        {
            std::cout << "ERROR: solution not correct!" << std::endl;
        }
    }
    auto stop = std::chrono::system_clock::now();
    auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();

    double dataSize = 1.0e-9 * static_cast<double>( sizeof(double) * ( M + M * N + N ) );

    std::cout << "runtime: " << runtime << " ms" << std::endl;
    std::cout << "memory bandwidth: " << 1e3 * dataSize * iterations / runtime << " GB/s" << std::endl;

    delete[] y;
    delete[] A;
    delete[] x;
}

int main(int argc, char** argv)
{
    yAx();
    return EXIT_SUCCESS;
}