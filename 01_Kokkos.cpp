#include <Kokkos_Core.hpp>
#include <chrono>
#include <iostream>
#include <vector>

/**
 * Perform a vector times matrix times vector multiplication.
 */
void yAx()
{
    // dimensions
    constexpr size_t M = 65536;
    constexpr size_t N = 1024;
    constexpr size_t iterations = 100;
    /// solution of the multiplication if all elements are initialized to 1
    double solution = M * N;

    // vector y in R^M
    auto y = Kokkos::View<double*>("y", M);
    // matrix A in R^MxN
    auto A = Kokkos::View<double**>("A", M, N);
    // vector x in R^N
    auto x = Kokkos::View<double*>("x", N);

    // ensure we have copies in host memory space
    auto h_y = Kokkos::create_mirror_view(Kokkos::HostSpace(), y);
    auto h_A = Kokkos::create_mirror_view(Kokkos::HostSpace(), A);
    auto h_x = Kokkos::create_mirror_view(Kokkos::HostSpace(), x);

    // initialization
    for (auto idx = 0; idx < M; ++idx)
    {
        h_y(idx) = 1;
    }
    for (auto idx = 0; idx < M; ++idx)
    {
        for (auto jdx = 0; jdx < N; ++jdx)
        {
            h_A(idx, jdx) = 1;
        }
    }
    for (auto jdx = 0; jdx < N; ++jdx)
    {
        h_x(jdx) = 1;
    }

    Kokkos::deep_copy(y, h_y);
    Kokkos::deep_copy(A, h_A);
    Kokkos::deep_copy(x, h_x);

    auto start = std::chrono::system_clock::now();
    for (auto i = 0; i < iterations; ++i)
    {
        double res = 0;
        auto policy = Kokkos::RangePolicy<>(0, M);
        auto kernel = KOKKOS_LAMBDA(const int64_t& idx, double& result)
        {
            double tmp = 0;
            for (auto jdx = 0; jdx < N; ++jdx)
            {
                tmp += A(idx,jdx) * x(jdx);
            }
            result += y(idx) * tmp;
        };
        Kokkos::parallel_reduce("yAx", policy, kernel, res);

        if (std::abs(res - solution) > 1e-8)
        {
            std::cout << "ERROR: solution not correct!" << std::endl;
        }
    }
    auto stop = std::chrono::system_clock::now();
    auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count();

    double dataSize = 1.0e-9 * static_cast<double>( sizeof(double) * ( M + M * N + N ) );

    std::cout << "runtime: " << runtime << " ms" << std::endl;
    std::cout << "memory bandwidth: " << 1e3 * dataSize * iterations / runtime << " GB/s" << std::endl;
}

int main(int argc, char** argv)
{
    Kokkos::initialize(argc, argv);
    std::cout << "execution space: " << typeid(Kokkos::DefaultExecutionSpace).name() << std::endl;
    yAx();
    Kokkos::finalize();
    return EXIT_SUCCESS;
}